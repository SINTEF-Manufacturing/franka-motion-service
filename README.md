# Building 

`libfranka` should be a compatible version. If outdated, go to the libfranka folder and[1]

    git pull
    git submodule update
    [mkdir|cd] build
    cmake -DCMAKE_BUILD_TYPE=Release ..
    cmake --build .

It is possible to also build and install a deb-package with 
    
    cpack -G DEB
    dpkg -i libfranka-<version>-<architecture>.deb
	aptitude install 

[1] https://frankaemika.github.io/docs/installation.html


Ensure dependencies are installed (maybe incomplete!):

    aptitude install libboost-system-dev

In the `franka_motion_service` folder: Ensure that `libfranka` is located as a sibling of the `franka_motion_service` folder. Alternatively, correct the include and link paths in the makefile and below. Then:

    make


# Performance setup


## CPU frequency scaling govenor

Ensure that `linux-cpupower` is installed:

    aptitude install linux-cpupower
    
Then, before running the motion interface service, set the frequency scaling govenor to `performance` on all cores:

    cpupower frequency-set --governor performance

## RT-scheduling

To improve latency in the kernel, an RT-preemptible kernel should be used.  The operating user should be given permissions to schedule processes in the real-time scheduler queues; `root` always have this priviledge. This may be accomplished by adding a `realtime` group to the system, and adding relevant users to this group. Then give the necessary performance priviledges to the group by creating a new `limits`-file `/etc/security/limits.d/realtime.conf` with the following contents:

    @realtime - rtprio 99
    @realtime - priority 99
    @realtime - memlock 1000000

It is very important that the allowed size of locked memory, denoted `memlock` and given in `kB`, is high enough. Otherwise, the RT-process will be forced to the penalty from page faults.

Starting a process at highest real-time scheduling priority (99) in the FIFO scheduler queue (`-f`) is done by prefixing the command line command by `chrt -f 99 [...]`.

Newer (6.x.x) kernels have dynamic preemtion (PREEMPT_DYNAMIC), which means that they can be hot-switched between voluntary and full preeption. Inspect and control by the sys-file `/sys/kernel/debug/sched/preempt`.


## Disk-IO performance

With `ionice` the disk performance can be tuned. Only root may set it to real-time priority (`-c 1`). Under normal circumstances, there should be no disk IO for the motion service. If large amounts of trajectory data are dumped to disk from the process for debugging, worst case latency penalties by disk IO may be slightly less if launched as root with the prefix `ionice -c 1 [...]`.


# Emika Controller 


For interacting with the Franka controller over FCI, the joints need to be unlocked. Connect to the Franka controller and log in with the correct username and password. Locate the button saying "Click to unlock joints", and press it. 


# Usage of franka\_motion\_service 


The optional argument `<physical robot host>` determines whether an emulator runs or the real robot controller is connected to.

    franka_motion_service <TG-client service port> <micro cycle time [us]> <micro cycles per macro cycle> [<physical robot host>]


The process should be run with real-time sheduling and real-time io-priority. Use `ionice -c 1 chrt -f 1 <command> ...` to start the process on `<command>`.


# Example 

For running, the path to the location of `libfranka.so` is needed in `LD_LIBRARY_PATH`. Set it up by using the `libfranka_base` path used in the makefile. Then issue
	
	export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${libfranka_base}/build/

Run the emulator at 0.01 s micro cycle time, 10 micro cycles per macro cycle, and servicing a trajectory generator on port 9999.

    chrt -f 1 ./franka_motion_service 9999 10000 10 


Running the physical robot set up with IP address 10.0.0.<N>:

    chrt -f 1 ./franka_motion_service 9999 1000 10 10.0.0.<N>


The included example client, `client.py`, generates an infinite cosine motion on joint 4, indexed from 0. Execute it with

    chrt -f 1 python3 -i client.py

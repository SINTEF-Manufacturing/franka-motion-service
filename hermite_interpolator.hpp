#pragma once

// Copyright: SINTEF Manufacturing 2018-2022
// License: LGPLv3
// Created by: Morten Lind
// eaddress: morten.lind@sintef.no


#include <valarray>
#include <stdexcept>

using namespace std;


/*double h_3_0(double t){
  return 1 - 3 * pow(t, 2) + 2 * pow(t,3);
  }
*/


class CubicHermite {
  // https://www.rose-hulman.edu/~finn/CCLI/Notes/day09.pdf

private:
  valarray<double> h3(const double u){
    double u2 = u * u, u3 = u2 * u;
    return {1 - 3 * u2 + 2 * u3,
	    u - 2 * u2 + u3,
	    -u2 + u3,
	    3 * u2 - 2 * u3};
  }
  
public:
  double t0, t1, dt;
  valarray<double> p0, p1, v0, v1, N;
  bool strict;

public:

  CubicHermite(){}
    
  CubicHermite(const double t0, const double t1,
	       const valarray<double> p0, const valarray<double> p1,
	       const valarray<double> v0, const valarray<double> v1,
	       bool strict=true) {
    setup(t0, t1, p0, p1, v0, v1, strict);
  }

  void setup(const double t0, const double t1,
	     const valarray<double> p0, const valarray<double> p1,
	     const valarray<double> v0, const valarray<double> v1,
	     bool strict=true) {
    this->t0 = t0;
    this->t1 = t1;
    dt = t1 - t0;
    this->p0 = p0;
    this->p1 = p1;
    this->v0 = v0;
    this->v1 = v1;
    this->N = p0.size();
    this->strict = strict;
  }
    
  valarray<double> operator()(const double t) {
    if (strict && (t < t0 || t>t1)) {
      throw std::invalid_argument(std::string("Value of t (")+
				  std::to_string(t) +
				  std::string(") is outside boundaries [t0;t1] ([") +
				  std::to_string(t0) + std::string(";") +
				  std::to_string(t1) + std::string("])!"));
    }
    valarray<double> h3u = h3((t-t0) / dt);
    return h3u[0] * p0 + h3u[1] * dt * v0 + h3u[2] * dt * v1 + h3u[3] * p1;
  }

  valarray<valarray<double>> operator()(const valarray<double> ts) {
    valarray<valarray<double>> ps(ts.size());
    int i;
    for(i=0; i<ts.size(); i++) {
      ps[i] = operator()(ts[i]);
    }
    return ps;
  }
};



class QuinticHermite {
  // https://www.rose-hulman.edu/~finn/CCLI/Notes/day09.pdf
private:

  valarray<double> h5(const double u){
    double u2 = u * u, u3 = u2 * u, u4 = u3 * u, u5 = u4 * u;
    return {1.0 - 10.0 * u3 + 15.0 * u4 - 6.0 * u5,
	    u  - 6.0 * u3 + 8.0 * u4 - 3.0 * u5,
	    0.5 * u2 - 1.5 * u3 + 1.5 * u4 - 0.5 * u5,
	    0.5  * u3 - u4 + 0.5 * u5,
	    -4.0 * u3 + 7.0 * u4 - 3.0 * u5,
	    10.0 * u3 - 15.0 * u4 + 6.0 * u5};
  }
    
public:
  double t0, t1, dt, dt2;
  valarray<double> p0, p1, v0, v1, a0, a1;
  int N;
  bool strict;

  QuinticHermite(){}
    
  QuinticHermite(const double t0, const double t1,
		 const valarray<double> p0, const valarray<double> p1,
		 const valarray<double> v0, const valarray<double> v1,
		 const valarray<double> a0, const valarray<double> a1,
		 bool strict=true) {
    setup(t0, t1, p0, p1, v0, v1, a0, a1, strict);
  }

  
  void setup(const double t0, const double t1,
	     const valarray<double> p0, const valarray<double> p1,
	     const valarray<double> v0, const valarray<double> v1,
	     const valarray<double> a0, const valarray<double> a1,
	     bool strict=true) {
    std::cout.flush();
    this->t0 = t0;
    this->t1 = t1;
    dt = t1 - t0;
    dt2 = dt * dt;
    this->dt = dt;
    this->dt2 = dt2;
    this->p0 = p0;
    this->p1 = p1;
    this->v0 = v0;
    this->v1 = v1;
    this->a0 = a0;
    this->a1 = a1;
    N = p0.size();
    this->strict = strict;
  }

  valarray<double> operator()(const double t) {
    if (strict && (t < t0 || t > t1)) {
      throw std::invalid_argument(std::string("Value of t (")+
				  std::to_string(t) +
				  std::string(") is outside boundaries [t0;t1] ([") +
				  std::to_string(t0) + std::string(";") +
				  std::to_string(t1) + std::string("])!"));
    }
    valarray<double> h5u = h5((t - t0) / dt);
    return h5u[0] * p0 + h5u[1] * dt * v0 + h5u[2] * dt2 * a0 +  h5u[3] * dt2 * a1 + h5u[4] * dt * v1 + h5u[5] * p1;
  }

  valarray<valarray<double>> operator()(const valarray<double> ts) {
    valarray<valarray<double>> ps(ts.size());
    int i;
    for(i=0; i<ts.size(); i++) {
      ps[i] = operator()(ts[i]);
    }
    return ps;
  }
};

# coding=utf-8

"""
Script for visualizing the exponential filtering effect for simple, linear mic-mac interpolation.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF 2018"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"

import sys
import argparse

import numpy as np
import matplotlib.pyplot as plt

ap = argparse.ArgumentParser()
ap.add_argument('--pdf', action='store_true')
ap.add_argument('alpha', type=float)
args = ap.parse_args()

dt_mic = 0.001
mpm = 10
dt_mac = mpm * dt_mic

dT = 5

ts = np.arange(0, dT, dt_mic)

a = 0.1 * np.pi
w = 0.5 * np.pi
def tg(t):
    return a * (1 - np.cos(w*t))

def va(tr, dt):
    p0, p1, p2 = np.array(tr[-3:])[:,1]
    v2 = (p2-p1) / dt
    v1 = (p1-p0) / dt
    a = (v2 - v1) / dt
    return v2, a

mic_va = lambda : va(mictr, dt_mic)
mac_va = lambda : va(mactr, dt_mac)
    
push_lat = False
alpha = args.alpha
mictr = [[-2*dt_mic,0], [-dt_mic,0]]
mictva = []
mactr = [[-2*dt_mac,0], [-dt_mac,0]]
mactva = []
macc = 0
micc = 0
prev_tgt = 0
tgt = 0
cur = 0
d = (tgt - cur) / mpm
d_act = 0
for t in ts:
    micc+=1
    new_mac = not bool(micc%mpm)
    micc %= mpm
    if new_mac:
        prev = tgt
        t_mac = t
        if push_lat:
            t_mac += dt_mac
        tgt = tg(t_mac)
        mactr.append((t_mac,tgt))
        mactva.append((t_mac, *mac_va()))
        d = (tgt - cur) / mpm
    d_act = (1-alpha) * d_act + alpha * d
    cur += d_act
    mictr.append((t, cur))
    mictva.append((t,*mic_va()))

marker = 'o'
if args.pdf:
    marker_size = 0.0
    line_width = 0.05
else:
    marker_size = 3
    line_width = 1
plt.suptitle('$A = %.2f\\,rad$ , $\\omega = %.2f\\,rad/s$ , $\\alpha = %.2f$'%(a,w,alpha))
plt.subplot(311)
plt.plot(*np.transpose(mictr), linestyle='-', marker=marker, color='red',
         label='Micro', markersize=marker_size, linewidth=line_width)
plt.plot(*np.transpose(mactr), marker=marker, color='blue',
         label='Macro', markersize=marker_size, linewidth=line_width)
plt.ylabel('Joint position [$rad$]')
plt.legend()
plt.grid()

plt.subplot(312)
plt.plot(*np.transpose(mictva)[[0,1],:], color='red', linestyle='-', marker=marker,
         label='Micro', markersize=marker_size, linewidth=line_width)
plt.plot(*np.transpose(mactva)[[0,1],:], color='blue', marker=marker,
         label='Macro', markersize=marker_size, linewidth=line_width)
plt.ylabel('Joint velocity [$rad\,s^{-1}$]')
plt.legend()
plt.grid()

plt.subplot(313)
plt.plot(*np.transpose(mictva)[[0,2],:], color='red', linestyle='-', marker=marker,
         label='Micro', markersize=marker_size, linewidth=line_width)
plt.plot(*np.transpose(mactva)[[0,2],:], color='blue', marker=marker,
         label='Macro', markersize=marker_size, linewidth=line_width)
plt.ylabel('Joint acceleration [$rad\,s^{-2}$]')
plt.legend()
plt.grid()


#plt.tight_layout(True)
if args.pdf:
    plt.savefig('mac-mic-analysis-alpha=%.2f.pdf' % alpha)
else:
    plt.show(block=False)

# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF 2018"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"

import time
import atexit
import argparse
import threading
import sys

import math3d as m3d
import numpy as np
import pymoco.controller_manager
import force_sensor
#import matplotlib.pyplot as plt


# Ensure thread switching with high time resolution. (System default
# may be as high as 5e-3)
sys.setswitchinterval(1e-5)

ap = argparse.ArgumentParser()

np.set_printoptions(precision=2, sign=' ', suppress=True, floatmode='fixed')

cm = pymoco.controller_manager.ControllerManager(rob_type='Emika', facade_name='FMSFacade', rob_host='127.0.0.1', rob_port=9999, cycle_time=0.01)
atexit.register(cm.stop)
rf = cm.robot_facade
fc = rf.frame_computer

fs = None
def get_fs():
    global fs
    if fs is None:
        fs = force_sensor.EmikaFTEstimator('localhost', 10000, timeout=0.15)
    return fs

#rf.robot_definition._spd_lim_act *= 0.3


class FTSampler(threading.Thread):
    def __init__(self, ft_sensor, cycle_time=None):
        """Sample the 'ft_sensor' in 'cycle_time'. If 'cycle_time' is None,
        sample every sample from 'ft_sensor'.
        """
        threading.Thread.__init__(self)
        self.daemon = True
        self.fts = ft_sensor
        self.ct = cycle_time
        self._samples = []
        self.__stop = None

    def get_samples(self):
        return np.array(self._samples)

    def stop(self):
        self.__stop = True

    def run(self):
        self.fts.sample_bias()
        self.__stop = False
        t_start = time.time()
        cycle = 0
        while not self.__stop:
            if self.ct is None:
                # Wait for next sample, then sample the wall clock time
                ft = self.fts.get_ft(wait=True)
                t = time.time()
            else:
                # Sample and calculate sample time
                ft = self.fts.ft
                t = t_start + cycle * self.ct
                cycle += 1
            # Record the sample
            self._samples.append(np.append([t], ft))
            if self.ct is not None:
                # Sleep until next cycle if specific cycle time
                time.sleep(t_start + cycle * self.ct - time.time())


def sample_fs(duration_s=2):
    fs = get_fs()
    #2*rf.control_period)
    time.sleep(1)
    fs.sample_bias()
    samples = []
    t0 = time.time()
    while True:  # Break after duration
        t = time.time() - t0
        samples.append(np.append([t],fs.get_ft(wait=True)))
        if t >= duration_s:
            break
    return np.array(samples)


def start_cc():
    global fs
    tvc = cm.tvc()
    time.sleep(0.1)
    tvc.set_twist(np.zeros(6), attach_frame='Base', express_frame='Tool', acc_limits=(1.0, 2.0))
    fs = get_fs()
    time.sleep(1)
    fs.sample_bias()
    # Force-torque received
    ft = np.zeros(6, dtype=np.float64)
    # Tool twist to command
    tt = np.zeros(6, dtype=np.float64)
    # Tool twist for actual commanding, filtered over tt.
    alpha_tt = 0.1
    alpha_ft = 0.1
    pg_v = 0.01 * np.ones(3)
    pg_w = 0.5 * np.ones(3)
    tta = np.zeros(6, dtype=np.float64)
    while True:
        cm.robot_facade.wait_for_control()
        # Input filtering of the measured FT
        meas_ft = fs.ft
        ft = (1-alpha_ft) * ft + alpha_ft * meas_ft
        # print(meas_ft)
        tt[:3] = np.clip(pg_v * ft[:3], -0.5, 0.5)
        tt[3:] = np.clip(pg_w * ft[3:], -3.0, 3.0)
        # tool-XY-compliance: tt[:2] = np.clip(pg_v * ft[:3], -1.0, 1.0)[:2]
        # Output filtering on the generated tool twist target
        tta = (1-alpha_tt) * tta + alpha_tt * tt
        tvc.target_twist = tta
    return


def move(dist=0.01, speed=0.01, dir=m3d.FreeVector(0, 0, 1), ref='Base'):
    tlc = cm.tlc(linear_speed=speed, angular_speed=speed)
    dir.normalize()
    p = fc.tool_pose
    if ref == 'Tool':
        dir = p.orient * dir
    p.pos += dist * dir
    tlc.set_target_pose(p, linear_speed=speed, angular_speed=speed)
    return


def force_adaptive_cutting():
    pass


def cutting(shear_stroke=0.02, cut_per_stroke=0.005, cut_depth=0.04, speed=0.05, log_fs=True, log_traj=True):
    """Perform a pre-defined sawing-type cutting process with a shearing
    distance'shear_stroke' and cutting distance 'cut_per_stroke' in
    every stroke. The cutting process ends when a total travel
    'cut_depth' in the cutting direction has been achieved. The
    'speed' of motion is used on every stroke. If 'log_fs' is True,
    the forces are sampled asynchronously with the entire motion.
    """
    global tp, cut_dir, shear_dir
    # Get a tool path linear controller
    tlc = cm.tlc(ramp_accel=0.75)
    # Sample the tool pose
    tp0 = fc.tool_pose
    tp = tp0.copy()
    # Cut direction is normal and outward to the edge in the blade plane
    cut_dir = -(tp.orient.vec_x + tp.orient.vec_y).normalized
    # Shear direction is along the edge toward the point
    shear_dir = (tp.orient.vec_x - tp.orient.vec_y).normalized
    # Calculate the number of strokes
    n_strokes = int(np.floor(cut_depth/cut_per_stroke))
    print('Cutting {} strokes'.format(n_strokes))
    # Do not start immediately
    input('Press "enter" for continuing')
    if log_fs:
        # Set up for force-torque logging in 100 Hz
        fs = get_fs()
        fts = FTSampler(fs)
        fts.start()
        # Sample unloaded at rest
        time.sleep(0.5)
    if log_traj:
        traj = []
    # Main loop executing the strokes
    for i in range(n_strokes):
        if log_traj:
            traj.append(np.append([time.time()], tp.pos.array))
        # Update the target tool pose for the forward stroke and execute
        tp.pos += shear_stroke * shear_dir + 0.5 * cut_per_stroke * cut_dir
        tlc.set_target_pose(tp, linear_speed=speed)
        tlc.wait_for_idle()
        if log_traj:
            traj.append(np.append([time.time()], tp.pos.array))
        # Update the target tool pose for the return stroke and execute
        tp.pos += -shear_stroke * shear_dir + 0.5 * cut_per_stroke * cut_dir
        tlc.set_target_pose(tp, linear_speed=speed)
        tlc.wait_for_idle()
        if log_traj:
            traj.append(np.append([time.time()], tp.pos.array))
    if log_fs:
        # Stop and return the force-torque sampler
        fts.stop()
        np.save('t_ft.npy', fts.get_samples())
    if log_traj:
        np.save('tool_pose_0.npy', tp0.array)
        np.save('cut_dir.npy', cut_dir.array)
        np.save('shear_dir.npy', shear_dir.array)
        np.save('cutting_traj.npy', np.array(traj))

# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2019-2021"
__credits__ = ["Morten Lind"]
__license__ = "AGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import time

import numpy as np
import pymoco.controller_manager

np.set_printoptions(precision=2, sign=' ', suppress=True, floatmode='fixed')

cm = pymoco.controller_manager.ControllerManager(
    rob_type='Emika', facade_name='FMSFacade',
    rob_host='localhost', rob_port=9999)
rf = cm.robot_facade
fc = rf.frame_computer
rf.robot_definition._spd_lim_act *= 0.3


def measure_pos_devs():
    N = 100
    a = np.empty((N, 3))
    for i in range(N):
        rf.wait_for_control()
        a[i] = fc().pos.array
    aa = np.average(a, axis=0)
    ad = a-aa
    print('plus-minus position span [um]: {}'
          .format(1e6*(ad.max(axis=0)-ad.min(axis=0))))


def step_cycle(step_size=10e-6, speed=None):
    if speed is None:
        speed = step_size
    p = fc()
    while True:
        p.pos.y += step_size
        print('going +')
        tlc.set_target_pose(p, linear_speed=speed)
        tlc.wait_for_idle()
        time.sleep(0.5)
        p.pos.y += step_size
        print('going -')
        tlc.set_target_pose(p, linear_speed=speed)
        tlc.wait_for_idle()
        time.sleep(0.5)


def step_fwd(step_size=10e-6, speed=None):
    if speed is None:
        speed = step_size
    p = fc()
    while True:
        p.pos.y += step_size
        print('going +')
        tlc.set_target_pose(p, linear_speed=speed)
        tlc.wait_for_idle()
        time.sleep(1.0)


tlc = cm.tlc(stopping_accuracy=1e-6, ramp_accel=0.2)
step_size = 10e-6
speed = 10e-6
p = fc().copy()
p.pos.y -= step_size
# tlc.set_target_pose(p, linear_speed=speed)

#pragma once

// Copyright: SINTEF Manufacturing 2018-2022
// License: LGPLv3
// Created by: Morten Lind
// eaddress: morten.lind@sintef.no

#include <valarray>
#include <boost/asio.hpp>
#include <franka/robot_state.h>


using namespace std;

class RobotEmulator{
private:
    bool debug=false;
    boost::asio::io_service& io_service;
    uint32_t mic_cycle_time_us;
    // hrtimer hrt;
    franka::RobotState rob_state;
    double alpha;
    valarray<double> mg_tpos, mc_tpos;
public:
    RobotEmulator(boost::asio::io_service& io_service, uint32_t mic_cycle_time_us) :
        io_service(io_service),
        // hrt(io_service),
        mic_cycle_time_us(mic_cycle_time_us),
        alpha(0.25)
    {
        //for(auto i=0;i<7;i++) fci_state.q[i] = 0.191293120938109283091283;
        rob_state.q_d = {1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7};
        mc_tpos = valarray<double>(rob_state.q_d.data(), 7);
    }

    franka::RobotState readOnce(){
        return rob_state;
    }
    
    void control(std::function<franka::JointPositions(const franka::RobotState&, franka::Duration)> motion_generator_callback){
        franka::Duration fci_time(0),last_fci_time(0), cycle_inc(mic_cycle_time_us/1000);
        // chrono::time_point<chrono::high_resolution_clock> start_tp(chrono::high_resolution_clock::now());
        chrono::time_point<chrono::high_resolution_clock> wakeup_tp = start_tp; // + 0ns;;
        array<double,7> mg_atpos;
        double start_ts = pts();
        double wakeup_ts = start_ts;
        double cb_dt_max = 0;
        uint32_t wakeup_error_us, max_wakup_error_us = 0;
        for(; ; fci_time += cycle_inc){
            if (debug) cout << "Cycle" << endl;
            wakeup_tp += chrono::microseconds(mic_cycle_time_us);
            this_thread::sleep_until(wakeup_tp);
            //wakeup_ts += 1e-6 * mic_cycle_time_us;
            //if (debug) cout << "Duration to wait for cycle: " << int((wakeup_ts - pts()) / 1e-6) << " us" << endl;
            //this_thread::sleep_until(tp_from_ts(wakeup_ts));
            //cout<<"FCIEmulator tick @ " << pts()-start_ts << endl;
            wakeup_error_us = chrono::duration_cast<chrono::microseconds>(chrono::high_resolution_clock::now() - wakeup_tp).count();
            if (wakeup_error_us > max_wakup_error_us) {
                max_wakup_error_us = wakeup_error_us;
                cout<<"Wake error [us]: " << wakeup_error_us << endl;
            }
            // chrono::duration_cast<chrono::microseconds>(chrono::high_resolution_clock::now()-starttime).count() * 1e-9<<endl;
            double t0 = pts();
            try {
                mg_atpos = motion_generator_callback(rob_state, fci_time-last_fci_time).q;
            }
            catch (...){
                cout << "Some exception happened in motion callback" << endl;
                exit(0);
            }
            double t1 = pts();
	    // cout<<t1-t0<<endl;
            if (t1 - t0 > cb_dt_max) {
                cb_dt_max = t1 - t0;
                cout << "Callback computation time : " << t1 - t0 << "s" << endl;
            }
            mg_tpos = valarray<double>(mg_atpos.data(), 7);
            mc_tpos = alpha * mg_tpos + (1-alpha) * mc_tpos;
            copy(begin(mc_tpos), end(mc_tpos), begin(rob_state.q_d));
            copy(begin(mc_tpos), end(mc_tpos), begin(rob_state.q));
            last_fci_time = fci_time;
            //cout << "Remaining time: " << hrt.expires_from_now() << endl;
        }
    }
};
